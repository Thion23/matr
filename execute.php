﻿<?php
$content = file_get_contents("php://input");
$update = json_decode($content, true);

if(!$update)
{
  exit;
}

function sendMessage($chatID, $messaggio, $token)
{
    echo "sending message to " . $chatID . "\n"; $url = "https://api.telegram.org/" . $token . "/sendMessage?chat_id=" . $chatID;
    $url = $url . "&text=" . urlencode($messaggio);
    $ch = curl_init();
    $optArray = array( CURLOPT_URL => $url, CURLOPT_RETURNTRANSFER => true );
    curl_setopt_array($ch, $optArray);
    $result = curl_exec($ch);
    curl_close($ch);
}

//Data un array di stringhe, questa funzione crea un'espressione regolare con tutte le permutazioni delle stringhe.
function permRegex($a)
{
    $str = "/(\b([A-z|']{2,} )?"; //Inizializza la stringa.
    
    if (sizeof($a) == 1) //Se e' uno, c'e' poco da permutare.
    {
        $str.=$a[0];
    }
    elseif (sizeof($a) == 2) //Le permutazioni sono n!
    {
        $str .= $a[0] . " " . $a[1] . "\b|\b" . $a[1] . " " . $a[0];
    } 
    elseif (sizeof($a) == 3) 
    {
        $str .= $a[0] . " " . $a[1] . " " . $a[2] . "\b|\b" .
        $a[0] . " " . $a[2] . " " . $a[1] . "\b|\b" .
        $a[1] . " " . $a[2] . " " . $a[0] . "\b|\b" .
        $a[1] . " " . $a[0] . " " . $a[2] . "\b|\b" .
        $a[2] . " " . $a[1] . " " . $a[0] . "\b|\b" .
        $a[2] . " " . $a[0] . " " . $a[1];
    }
    elseif (sizeof($a) > 3)
    {
        $str .= $a[0] . " " . $a[1] . " " . $a[2] . " " . $a[3] . "\b|\b" .
        $a[0] . " " . $a[1] . " " . $a[3] . " " . $a[2] . "\b|\b" .
        $a[0] . " " . $a[2] . " " . $a[1] . " " . $a[3] . "\b|\b" .
        $a[0] . " " . $a[2] . " " . $a[3] . " " . $a[1] . "\b|\b" .
        $a[0] . " " . $a[3] . " " . $a[1] . " " . $a[2] . "\b|\b" .
        $a[0] . " " . $a[3] . " " . $a[2] . " " . $a[1] . "\b|\b" .
        $a[1] . " " . $a[0] . " " . $a[2] . " " . $a[3] . "\b|\b" .
        $a[1] . " " . $a[0] . " " . $a[3] . " " . $a[2] . "\b|\b" .
        $a[1] . " " . $a[2] . " " . $a[0] . " " . $a[3] . "\b|\b" .
        $a[1] . " " . $a[2] . " " . $a[3] . " " . $a[0] . "\b|\b" .
        $a[1] . " " . $a[3] . " " . $a[0] . " " . $a[2] . "\b|\b" .
        $a[1] . " " . $a[3] . " " . $a[2] . " " . $a[0] . "\b|\b" .
        $a[2] . " " . $a[0] . " " . $a[2] . " " . $a[3] . "\b|\b" .
        $a[2] . " " . $a[0] . " " . $a[3] . " " . $a[2] . "\b|\b" .
        $a[2] . " " . $a[2] . " " . $a[0] . " " . $a[3] . "\b|\b" .
        $a[2] . " " . $a[2] . " " . $a[3] . " " . $a[0] . "\b|\b" .
        $a[2] . " " . $a[3] . " " . $a[0] . " " . $a[2] . "\b|\b" .
        $a[2] . " " . $a[3] . " " . $a[2] . " " . $a[0] . "\b|\b" .
        $a[3] . " " . $a[0] . " " . $a[2] . " " . $a[3] . "\b|\b" .
        $a[3] . " " . $a[0] . " " . $a[3] . " " . $a[2] . "\b|\b" .
        $a[3] . " " . $a[2] . " " . $a[0] . " " . $a[3] . "\b|\b" .
        $a[3] . " " . $a[2] . " " . $a[3] . " " . $a[0] . "\b|\b" .
        $a[3] . " " . $a[3] . " " . $a[0] . " " . $a[2] . "\b|\b" .
        $a[3] . " " . $a[3] . " " . $a[2] . " " . $a[0];
    }
    
    $str .= "( [A-z|']{2,})?( [A-z|']{2,})?\b)/"; //Si chiude la stringa
    return $str; //Nota: Vengono aggiunte altre parole al pattern per fare in modo che trovi anche i risultati parziali.
}

//Data una riga, questa funzione separa la matricola dal nome e restituisce la stringa pronta all'uso.
function rispondi($line)
{
    preg_match("/^(\d(\d)?\/\d(\d)?\/)?\d\d\d\d(\d)?/", $line, $matr); //Prende la matricola completa di corso di studi
    preg_match("/[A-z|']{2,} [A-z|']{2,}( [A-z|']{2,})*/",$line,$nome); //Prende il nome
    preg_match("/\d\d\/\d\d\/\d\d\d\d$/",$line,$data); //Prende la data di nascita
    if ($data[0])
        return "Studente: ".$nome[0]."\nMatricola: ".$matr[0]."\n"."Data di nascita: ".$data[0]."\n";
    else
        return "Studente: ".$nome[0]."\nMatricola: ".$matr[0]."\n";
}

//Data una matricola, questa funzione restituisce il corso di studi.
function corso($line)
{
    $cds=fopen("corso.txt","r"); //Apre il file dei corsi di studio
    preg_match("/^\d(\d)?\/\d(\d)?/",$line,$corso); //Prende la parte di matricola che interessa
    while(($linea=fgets($cds))!==false) //Controlla riga per riga
    {
        preg_match("/^\d(\d)?\/\d(\d)?/",$linea,$confronto); //Prende la matricola dalla riga appena controllata
        if ($confronto[0]==$corso[0]) //Se e' vero, ha trovato il nome del corso
        {
            preg_match("/[A-z][A-z- ']*/",$linea,$nome); //Prende solo il corso...
            fclose($cds); //chiude il file prima del ritorno
            return "Corso di studi: ".$nome[0]."\n"; // ... e la aggiunge alla risposta.
        } //chiude l'if
    } //chiude il while
} //chiude la function  


function docente($line)
{
    preg_match("/\d\d\/\d\d\/\d\d\d\d/", $line, $data); //Prende la data di nascita
    preg_match("/^[A-z|']{2,} [A-z|']{2,}( [A-z|']{2,})*/",$line,$nome); //Prende il nome
    preg_match("/Dipartimento [A-z ']*$/",$line,$dipartimento);
    return "Docente: ".$nome[0]."\nData di nascita: ".$data[0]."\n".$dipartimento[0]."\n";
}

//Roba varia di default per il corretto utilizzo del bot
$message = isset($update['message']) ? $update['message'] : "";
$messageId = isset($message['message_id']) ? $message['message_id'] : "";
$chatId = isset($message['chat']['id']) ? $message['chat']['id'] : "";
$firstname = isset($message['chat']['first_name']) ? $message['chat']['first_name'] : "";
$lastname = isset($message['chat']['last_name']) ? $message['chat']['last_name'] : "";
$username = isset($message['chat']['username']) ? $message['chat']['username'] : "";
$date = isset($message['date']) ? $message['date'] : "";
$text = isset($message['text']) ? $message['text'] : "";

//Inizializzazione del messaggio di log
$inoltro="Messagio da: ".$firstname." ".$lastname." (".$username.")\n***\n".$text."\n***\n";

//Inizializzazione del messaggio in ingresso per poterci lavorare sopra
$text = trim($text); //A quanto ho capito, ma non ne sono sicuro, toglie gli spazi prima e dopo la stringa.
$text = strtoupper($text); //Mette in maiuscolo

//Inizializza la risposta
$response="";

//Inizializzazione dei pattern per le espessioni regolari
$pMatricola="/(^(\d(\d)?\/\d(\d)?\/)?\d\d\d\d(\d)?)/"; //Il pattern di una matricola
$pNome="/([A-z|']{2,}( [A-z|']{2,})*)/"; //Il pattern di un Nome

$cio=3900; //Numero massimo di caratteri: telegram supporta massimo 4096 caratteri per messaggio.

//Easter egg
if($text=="IL SOMMO" || $text=="SOMMO")
{
    $text="GIANNI FENU";
}

//Easter egg
if($text=="CONTE SWITCH")
{
    $text="CARTA SALVATORE";
}

//Easter egg
if($text=="DJANNI")
{
    $response.="Djanni si ricorderà di te quando conquisterà il mondo."; //Autoesplicativo.
}

//Funzione che indica chi e' autorizzato a utilizzare il bot. Nota: e' pubblico.
elseif ($text=="/CHATID")
{
    $pf=fopen("chatId.txt","r"); //Apre il file delle autorizzazioni
    while(($line=fgets($pf))!==false) //Legge riga per riga...
    {
        $response.=$line."\n"; //... e lo mette nella risposta.
    }
    fclose($pf); //Chiude il file.
} //Chiuso l'elseif, va direttamente alla fine per mandare i messaggi.

//La vera essenza del bot e' qui.
else
{
    //Controlla se la persona e' autorizzata.
    $accesso=fopen("chatId.txt","r"); //Apre il file delle autorizzazioni
    while(($test=fgets($accesso))!==false) //Controlla riga per riga e la mette in test
    {
        preg_match("/(\d)*/",$test,$autorizzato); //Di quella riga prende solo il codice della chat
        if($autorizzato[0]==$chatId) //Se e' autorizzato, allora i due valori sono uguali
        {
            //Autorizzazione confermata
            
            //Inizializzazione
            $filematricole=fopen("Dati.txt","r"); //Apre il file delle matricole.
            $counter=0; //Inizializza il contatore dei risultati.
            $conteggio=0; //Inizializza il contatore delle matricole analizzate.
            
            //Analisi del testo inserito
            if (preg_match($pMatricola,$text,$respText)) //Ha inserito una matricola
            {
                $pMatr="/(" . str_replace("/", "\/", $respText[0]) . ")/"; //Crea il pattern con la matricola inserita
                
                while(($line=fgets($filematricole))!==false) //Controlla riga per riga e mette in line
                {
                    $conteggio++; //Ha analizzato un'altra matricola, quindi aumenta
                    if(preg_match($pMatr,$line)) //Se sono uguali c'e' corrispondenza
                    {
                        $response.=rispondi($line).corso($line)."\n"; //Aggiorna la risposta con Nome e Matricola
                        $counter++; //Aumenta il numero dei risultati.

                        //Il seguente controllo serve per evitare che superi il massimo numero di caratteri
                        if (strlen($response)>=$cio) //Se la lunghezza della risposta e' piu' grande del massimo...
                        {
                            $response.="\nTroppi risultati. Mostrati: ".$counter; //... manda un messaggio d'interruzione...
                            break; //... e smette di cercare.
                        } //chiude l'if del controllo caratteri
                    } //chiude l'if della corrispondenza
                } //chiude il while
            } //chiude il caso della matricola
            
            elseif(preg_match($pNome,$text,$respText)) //Ha inserito un nome
            {
                $arrStud=explode(" ",$respText[0]); //"Esplode" la stringa
                $pPermStud=permRegex($arrStud); //Crea tutte le permutazioni della stringa
                //Di fatto cio' che segue e' uguale al caso della matricola. Potevo fare una function, ma ormai era gia' scritta.
                while(($line=fgets($filematricole))!==false)
                {
                    $conteggio++;
                    if(preg_match($pPermStud,$line))
                    {
                        $response.=rispondi($line).corso($line)."\n";
                        $counter++;
                        
                        if (strlen($response)>=$cio)
                        {
                            $response.="\nTroppi risultati. Mostrati: ".$counter;
                            break;
                        } //chiude l'if di controllo caratteri                        
                    } //chiude l'if della corrispondenza
                    //Questo evita ricerche inutili
                    if (preg_match("/Non disponibile/",$line)) //Esclusione delle righe non ricercabili via nome
                    {
                       break;
                    } //chiude l'if di esclusione nomi non cercabili

                } //chiude il while
                
                //Docenti
                if (strlen($response)<$cio) //Se la risposta ha gia' superato il massimo, non controlla i docenti.
                {
                    $filedocenti=fopen("docenti.txt","r"); //Apre il file docenti
                    while(($line=fgets($filedocenti))!==false) //Prende una riga e la mette in line
                    {
                        $conteggio++;
                        if(preg_match($pPermStud,$line)) //Se e' vero, ha trovato corrispondenza
                        {
                            $response.=docente($line)."\n"; //Aggiorna la risposta
                            $counter++;
                        }
                    }
                    fclose($filedocenti); //Chiude il file
                }
            } //chiude il caso del nome

            else //Ha inserito qualcosa che non e' ne' un nome ne' una matricola
            {
                $response.="Non valido."; //Autoesplicativo
                break; //Evita di cercare nel file
            }

            fclose($filematricole); //Chiude il file.
            
            if ($response=="") //Se nonostante abbia inserito un nome o una matricola, la risposta e' ancora vuota, allora non ha trovato corrispondenze.
                {
                    $response.="Non in elenco.\n"; //Autoesplicativo
                }
            
            //Raggiunto questo punto, ha finito le ricerche.
            break; //Interrompe il ciclo della verifica delle autorizzazioni
        } //chiude il ciclo delle autorizzazioni
    }
    
    //Caso in cui non e' autorizzato
    if ($response=="") //Se questo rimane vuoto, allora non e' entrato nel ciclo. Implica che non e' autorizzato a usare il bot.
    {
        $response.="Non sei autorizzato ad usare il bot.\n"; //Autoesplicativo
        $counter=$chatId; //Nella chatId c'e' il numero per autorizzare l'utilizzo.
    }
    fclose($accesso); //Chiude il file delle autorizzazioni.
    $inoltro.="\nRisultati: ".$counter."/".$conteggio."."; //Chiude la risposta con le statistiche.
}

$inoltro.="\nRisposta:\n***\n".$response."\n***\nChat: ".$chatId."\nCaratteri: ".strlen($response); //chiusura messaggio di log

$token = "bot293607217:AAGzNtb_2uBOcECMCkBaFS0zvIUvlpjITT4"; //token del bot
sendMessage ($chatId,$response,$token); //invia messaggio alla chat
sendMessage(241547230,$inoltro,$token); //invia messaggio di log
